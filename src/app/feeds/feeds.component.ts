import { Component, OnInit } from '@angular/core';
import { FeedsService } from './feeds.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.component.html',
  styleUrls: ['./feeds.component.css']
})
export class FeedsComponent implements OnInit {
  __boardInfo;
  __currentLangCode;
  __boardAka;
  __boardStyle;
  constructor(
    private router: Router,
    private translate: TranslateService,
    private feedsService: FeedsService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.data.subscribe(data => {
      this.__boardInfo = data['boardInfo']['response'];
      this.__boardStyle = data['boardInfo']['response']['style'];
    });
    this.activatedRoute.queryParams.subscribe(qparams => {
      if (Object.keys(qparams).indexOf('lang') > -1) {
        this.__currentLangCode = qparams['lang'];
      } else {
        this.__currentLangCode = translate.currentLang;
      }
      for (const lang of this.__boardInfo['lang_code']) {
        if (lang.lang_code === this.__currentLangCode) {
          this.__boardAka = lang.aka;
        }
      }
    });
   }

  ngOnInit() {
  }

  changeLangCode($event) {
    const qparam = Object.assign({},
      this.activatedRoute.snapshot.queryParams,
      {
        lang: this.__currentLangCode
      });
    this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: qparam });
  }

}
