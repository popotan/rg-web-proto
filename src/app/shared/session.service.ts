import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from '../../environments/environment.prod';
import { AuthResponse } from 'ngx-facebook';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  public sessionFrom;
  public requestOn;
  public expiredIn;
  public reauthorizedRequiredIn;
  public userId;
  public status; // connected, not_authorized, unknown
  public commonSession;
  constructor(
    private $http: HttpClient
  ) { }
  initProperties() {
    this.sessionFrom = undefined;
    this.requestOn = undefined;
    this.expiredIn = undefined;
    this.reauthorizedRequiredIn = undefined;
    this.userId = undefined;
    this.status = undefined;
    this.commonSession = undefined;
  }

  hasAuth(authList: any[] = []): boolean {
    if (!this.commonSession) { return false; }
    for (let i = 0; i < this.commonSession['auth'].length; i++) {
      if (authList.indexOf(this.commonSession['auth'][i]) > -1) {
        return true;
      }
    }
    return false;
  }

  isArticleOwner(article_guest_id): boolean {
    if (!this.commonSession) { return false; }
    if (article_guest_id === this.commonSession['guest_id']) {
      return true;
    } else {
      return false;
    }
  }

  public get isAdmin(): boolean {
    if (!this.commonSession) { return false; }
    if (this.commonSession['auth'].indexOf('admin') > -1) {
      return true;
    } else {
      return false;
    }
  }

  getFacebookUserInfoFromGraphApi(authResponse: AuthResponse) {
    return this.$http.get(
      `https://graph.facebook.com/v2.11/${authResponse.userID}?`
      + `fields=id,name,email,picture.width(160).height(160).type(square)`
      + `&access_token=${authResponse.accessToken}`)
      .pipe();
  }
  postFacebookSession(responseFromFacebook) {
    return this.$http.post(apiURL + `/cuser/session/facebook`, responseFromFacebook)
      .pipe();
  }
  getSession() {
    return this.commonSession;
  }
  removeSession() {
    return this.$http.delete(apiURL + `/cuser/session`)
      .pipe();
  }
}
