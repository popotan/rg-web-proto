import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SessionService } from './session.service';

@NgModule({
  imports: [
    CommonModule
  ],
  exports : [
    TranslateModule
  ],
  declarations: [],
  providers: [SessionService]
})
export class SharedModule { }
