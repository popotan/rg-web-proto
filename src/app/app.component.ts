import { Component, ElementRef, PLATFORM_ID, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from './shared/session.service';
import { Router, NavigationEnd } from '@angular/router';
import { FacebookService, InitParams } from 'ngx-facebook';
import { MatSnackBar } from '@angular/material';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // title = 'rg-web-proto';
  sbRef;
  constructor(
    private el: ElementRef,
    private translate: TranslateService,
    private sessionService: SessionService,
    private facebookService: FacebookService,
    private router: Router,
    private snackBar: MatSnackBar,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    if (isPlatformBrowser(this.platformId)) {
      const initParams: InitParams = {
        appId: '2171267379816301',
        xfbml: true,
        version: 'v2.8'
      };

      facebookService.init(initParams);

      this.translate.onLangChange.subscribe($event => {
        const langattr = document.createAttribute('lang');
        langattr.value = $event.lang;
        this.el.nativeElement.parentElement.parentElement.attributes.setNamedItem(langattr);
      });
      this.setInitLang();

      this.sbRef = this.snackBar.open('Getting your web sessions.');

      // this.router.events.subscribe(result => {
      //   if (result instanceof NavigationEnd) {
          this.getLoginStatusFromFacebook();
      //   }
      // });
    }
  }

  getLoginStatusFromFacebook() {
    this.facebookService.getLoginStatus().then(response => {
      this.sessionService.status = response['status'];
      switch (response['status']) {
        case 'connected':
          this.sessionService.userId = response.authResponse['userID'];
          this.sessionService.expiredIn = response.authResponse['expiredIn'];
          this.sessionService.reauthorizedRequiredIn = response.authResponse['reauthorized_required_in'];
          this.sessionService.getFacebookUserInfoFromGraphApi(response['authResponse'])
            .subscribe(response2 => {
              this.sessionService.postFacebookSession(response2)
                .subscribe(response3 => {
                  if (response3['meta']['message'] === 'SUCCESS') {
                    this.sessionService.sessionFrom = 'facebook';
                    this.sessionService.commonSession = response3['response'];
                    this.sbRef.dismiss();
                  }
                });
            });
          break;
        default:
          this.sessionService.removeSession().subscribe(response2 => {
            if (response2['meta']['message'] === 'SUCCESS') {
              this.sessionService.sessionFrom = undefined;
              this.sessionService.commonSession = undefined;
              this.sessionService.status = response['status'];
              this.sbRef.dismiss();
            }
          });
          break;
      }
    }).catch();
  }

  setInitLang() {
    this.translate.addLangs(['en', 'ko']);

    const pastLang = localStorage.getItem('lang');

    if (pastLang) {
      this.translate.use(pastLang);
      this.translate.setDefaultLang(pastLang);
    } else {
      this.translate.use('ko');
      this.translate.setDefaultLang('ko');
    }
  }
}
