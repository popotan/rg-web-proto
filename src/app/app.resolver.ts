import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { SessionService } from './shared/session.service';
import { FacebookService } from 'ngx-facebook';

@Injectable()
export class AppResolver implements Resolve<any> {
  constructor(
    private sessionService: SessionService,
    private facebookService: FacebookService
  ) { }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    return this.getLoginStatusFromFacebook();
  }

  getLoginStatusFromFacebook() {
    return this.facebookService.getLoginStatus().then(response => {
      this.sessionService.status = response['status'];
      switch (response['status']) {
        case 'connected':
          this.sessionService.userId = response.authResponse['userID'];
          this.sessionService.expiredIn = response.authResponse['expiredIn'];
          this.sessionService.reauthorizedRequiredIn = response.authResponse['reauthorized_required_in'];
          return this.sessionService.getFacebookUserInfoFromGraphApi(response['authResponse'])
            .subscribe(response2 => {
              return this.sessionService.postFacebookSession(response2)
                .subscribe(response3 => {
                  if (response3['meta']['message'] === 'SUCCESS') {
                    this.sessionService.sessionFrom = 'facebook';
                    this.sessionService.commonSession = response3['response'];
                  }
                });
            });
        default:
          return this.sessionService.removeSession().subscribe(response2 => {
            if (response2['meta']['message'] === 'SUCCESS') {
              this.sessionService.sessionFrom = undefined;
              this.sessionService.commonSession = undefined;
              this.sessionService.status = response['status'];
            }
          });
      }
    }).catch();
  }
}
