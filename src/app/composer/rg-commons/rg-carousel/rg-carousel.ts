import { ElementRef } from '@angular/core';
export class RgCarousel implements abcRgCarousel.RgCarousel {
    domId: number;
    cssWidth: string;
    cssHeight: string;
    arrowVisible = true;
    // tslint:disable-next-line:max-line-length
    arrowPosition: { cssTop: string, cssLeft: string, cssRight: string } = { cssTop: '50%', cssLeft: '1rem', cssRight: '1rem' };
    navigationVisible = true;
    navigationPosition: { top, left };
    slideTimer: any = 4000; // millisecond
    __slide: ElementRef[];
    __currentPage: number;
    set slide(value: ElementRef[]) {
        this.__slide = value;
    }
    set currentPage(value: number) {
        this.__currentPage = value;
    }
    constructor() {
    }
}
