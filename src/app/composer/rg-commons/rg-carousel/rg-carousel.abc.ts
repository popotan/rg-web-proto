namespace abcRgCarousel {
    export interface Carousel {
        domId: number;
        cssWidth: string;
        cssHeight: string;
        slide: any[];
        currentPage: number;
    }

    export abstract class RgCarousel implements Carousel {
        domId: number;
        cssWidth: string;
        cssHeight: string;
        slideTimer: any;
        __slide: any[];
        set slide(value: any[]) { }
        get slide() {
            return this.__slide;
        }
        __currentPage: number;
        set currentPage(value) { }
        get currentPage() {
            return this.__currentPage;
        }
    }
}
