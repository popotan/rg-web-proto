import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import {TransferHttpCacheModule} from '@nguniversal/common';

import {
  MatButtonModule, MatMenuModule, MatTabsModule, MatDividerModule, MatFormFieldModule,
  MatInputModule, MatCardModule, MatListModule, MatDialogModule, MatCheckboxModule, MAT_DIALOG_DEFAULT_OPTIONS,
  MatSnackBarModule,
  MatRadioModule,
  MatToolbarModule,
  MatExpansionModule,
  MatAccordion
} from '@angular/material';
import { MatIconModule } from '@angular/material/icon';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import * as mat from '@angular/material';

import { FacebookModule } from 'ngx-facebook';

import { RgCommonsModule } from './rg-commons/rg-commons.module';

import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { SitemapComponent } from './sitemap/sitemap.component';
import { PurposeComponent } from './articles/purpose/purpose.component';
import { StructureComponent } from './articles/structure/structure.component';
import { OrganizationComponent } from './articles/organization/organization.component';
import { ContactUsComponent } from './articles/contact-us/contact-us.component';
import { BeWithComponent } from './articles/be-with/be-with.component';
import { BoardsComponent } from './boards/boards.component';
import { FeedsComponent } from './feeds/feeds.component';
import { LoginComponent } from './dialogs/login/login.component';
import { EditorComponent } from './boards/editor/editor.component';
import { ReaderComponent } from './boards/reader/reader.component';
import { HeaderInterceptorService } from './header-interceptor.service';

import { BoardsResolver } from './boards/boards.resolver';
import { ReaderResolver } from './boards/reader/reader.resolver';
import { AgreementComponent } from './articles/agreement/agreement.component';
import { PrivacyComponent } from './articles/privacy/privacy.component';
import { NoEmailComponent } from './articles/no-email/no-email.component';
import { AppResolver } from './app.resolver';
import { RgCarouselComponent } from './rg-carousel/rg-carousel.component';
import { SessionService } from './shared/session.service';
import { TruncatePipe } from './rg-commons/truncate.pipe';
import { RgNoticePopupComponent } from './rg-notice-popup/rg-notice-popup.component';

// on serve mode
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

// on --aot compile
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    NavbarComponent,
    FooterComponent,
    SitemapComponent,
    PurposeComponent,
    StructureComponent,
    OrganizationComponent,
    ContactUsComponent,
    BeWithComponent,
    BoardsComponent,
    FeedsComponent,
    LoginComponent,
    EditorComponent,
    ReaderComponent,
    AgreementComponent,
    PrivacyComponent,
    NoEmailComponent,
    RgCarouselComponent,
    RgNoticePopupComponent
  ],
  entryComponents: [
    LoginComponent
  ],
  imports: [
    // BrowserModule,
    BrowserModule.withServerTransition({ appId: 'rg-web-proto' }),
    BrowserAnimationsModule,
    // TransferHttpCacheModule,
    FormsModule, ReactiveFormsModule,
    MatButtonModule, MatMenuModule, MatTabsModule, MatDividerModule, MatFormFieldModule, MatInputModule,
    MatCardModule, MatListModule, MatDialogModule, MatCheckboxModule, MatIconModule, MatSnackBarModule,
    MatRadioModule, MatToolbarModule, MatExpansionModule,
    RgCommonsModule,
    HttpClientModule,
    FacebookModule.forRoot(),
    SharedModule,
    // on serve mode
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),

    // on --aot compile
    // TranslateModule.forRoot({
    //   loader: {
    //     provide: TranslateLoader,
    //     useFactory: (createTranslateLoader),
    //     deps: [HttpClient]
    //   }
    // }),

    RouterModule.forRoot([
      {
        path: '',
        component: IndexComponent
      }, {
        path: 'sitemap',
        component: SitemapComponent
      }, {
        path: 'article',
        children: [
          {
            path: 'purpose',
            component: PurposeComponent
          }, {
            path: 'structure',
            component: StructureComponent
          }, {
            path: 'organization',
            component: OrganizationComponent
          }, {
            path: 'be-with',
            component: BeWithComponent
          }, {
            path: 'contact-us',
            component: ContactUsComponent
          }, {
            path: 'agreement',
            component: AgreementComponent
          }, {
            path: 'privacy',
            component: PrivacyComponent
          }, {
            path : 'no-email',
            component: NoEmailComponent
          }
        ]
      }, {
        path: 'board',
        children: [
          {
            path: ':boardName',
            component: BoardsComponent,
            resolve: {
              boardInfo: BoardsResolver
            },
            children: [
              {
                path: 'article/:articleCid',
                component: ReaderComponent,
                resolve: {
                  articleInfo: ReaderResolver
                }
              }
            ]
          }, {
            path: ':boardName/:mode',
            component: EditorComponent,
            resolve: {
              boardInfo: BoardsResolver
            }
          }, {
            path: ':boardName/:mode/article/:articleCid',
            component: EditorComponent,
            resolve: {
              boardInfo: BoardsResolver
            }
          }
        ]
      }, {
        path: 'feed',
        children: [
          {
            path: ':feedName',
            component: FeedsComponent
          }
        ]
      }
    ])
  ],
  exports: [
    AppComponent
  ],
  providers: [
    SessionService,
    TruncatePipe,
    AppResolver,
    BoardsResolver,
    ReaderResolver,
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeaderInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
