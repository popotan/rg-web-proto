import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from '../../../environments/environment.prod';
import { FacebookService, AuthResponse } from 'ngx-facebook';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private $http: HttpClient,
    private facebookService: FacebookService
  ) { }

  postRedirectUrl(targetRedirectUrl) {
    return this.$http.post(apiURL + `/cuser/redirect_url`, { redirect_url: targetRedirectUrl })
      .pipe();
  }
}
