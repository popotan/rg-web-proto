import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LoginService } from './login.service';
import { FacebookService } from 'ngx-facebook';
import { MatSnackBar, MatDialogRef } from '@angular/material';
import { SessionService } from '../../shared/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {
  isRedirectUrlPosted = false;
  agree = false;
  constructor(
    private loginService: LoginService,
    private facebookService: FacebookService,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<LoginComponent>,
    private sessionService: SessionService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // this.isRedirectUrlPosted = false;
    this.isRedirectUrlPosted = true;
    const presentUrl = window.location.protocol + '//' + window.location.host + '/' + window.location.pathname;
    // this.loginService.postRedirectUrl(presentUrl)
    //   .subscribe(response => {
    //     this.isRedirectUrlPosted = true;
    //   });
  }

  facebookLogin() {
    if (this.agree) {
      this.facebookService.login({
        scope: 'public_profile,email',
        return_scopes: true,
        enable_profile_selector: true
      }).then(response => {
        switch (response['status']) {
          case 'connected':
            this.sessionService.status = response.authResponse['status'];
            this.sessionService.userId = response.authResponse['userID'];
            this.sessionService.expiredIn = response.authResponse['expiredIn'];
            this.sessionService.reauthorizedRequiredIn = response.authResponse['reauthorized_required_in'];

            this.sessionService.getFacebookUserInfoFromGraphApi(response['authResponse'])
              .subscribe(response2 => {
                this.sessionService.postFacebookSession(response2)
                  .subscribe(response3 => {
                    if (response3['meta']['message'] === 'SUCCESS') {
                      this.sessionService.sessionFrom = 'facebook';
                      this.sessionService.commonSession = response3['response'];
                      this.snackBar.open('Success log in, Welcome ' + this.sessionService.commonSession['name'], 'Done');
                      this.dialogRef.close();
                    }
                  });
              });
            break;
          case 'not_authorized':
            this.snackBar.open('Please agree to submit your infomation from facebook', 'Done');
            break;
          case 'unknown':
            this.snackBar.open('Please login to facebook account.', 'Done');
            break;
          default:
            this.snackBar.open('Woops, something want wrong, Sorry. Please retry.', 'Done');
            break;
        }
      }).catch();
    } else {
      this.snackBar.open('Please check our terms.', 'Done');
    }
  }
}
