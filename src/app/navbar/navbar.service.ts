import { Injectable } from '@angular/core';
import { apiURL } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  constructor(
    private $http: HttpClient
  ) { }

  getBoardList() {
    return this.$http.get(apiURL + `/board`).pipe();
  }
}
