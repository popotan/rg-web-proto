import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Renderer2, HostListener, Inject, PLATFORM_ID } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';
import { LoginComponent } from '../dialogs/login/login.component';
import { SessionService } from '../shared/session.service';
import { FacebookService } from 'ngx-facebook';
import { NavbarService } from './navbar.service';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, AfterViewInit {
  @ViewChild('navHeader') navHeader: ElementRef;
  @ViewChild('navi') navi;
  @ViewChild('options') options;
  isNaviShowing = false;
  boardList = [];
  constructor(
    private navbarService: NavbarService,
    private facebookService: FacebookService,
    private session: SessionService,
    private renderer2: Renderer2,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    if (isPlatformBrowser(this.platformId)) {
      this.router.events.subscribe((result) => {
        if (result instanceof NavigationEnd) {
          this.renderer2.removeStyle(this.navHeader.nativeElement, 'background-color');
          this.onResize({ target: { innerWidth: window.innerWidth } });
        }
      });
    }
  }

  ngOnInit() {
    this.getBoardList();
    if (window.innerWidth > 960) {
      this.isNaviShowing = true;
    }
  }
  ngAfterViewInit() {
    // this.renderer2.listen('document', 'scroll', $event => {
    //   if ($event.target.scrollingElement.scrollTop < 80) {
    //     const opacity = ($event.target.scrollingElement.scrollTop) / 80;
    //     this.renderer2.setStyle(this.navHeader.nativeElement, 'background-color', 'rgba(255, 255, 255,' + opacity + ')');
    //   } else {
    //     const opacity = 1;
    //     this.renderer2.setStyle(this.navHeader.nativeElement, 'background-color', 'rgba(255, 255, 255,' + opacity + ')');
    //   }
    // });
  }
  @HostListener('window:resize', ['$event'])
  onResize($event) {
    if ($event.target.innerWidth > 960) {
      if (!this.isNaviShowing) {
        this.isNaviShowing = true;
      }
    } else {
      if (this.isNaviShowing) {
        this.isNaviShowing = false;
      }
    }
  }
  showNavi() {
    this.isNaviShowing = !this.isNaviShowing;
  }
  changeLang(lang: string) {
    if (lang !== this.translate.currentLang) {
      this.translate.use(lang);
      this.translate.setDefaultLang(lang);
      localStorage.setItem('lang', lang);
      this.router.navigate([], { relativeTo: this.activatedRoute });
    }
  }
  getBoardList() {
    this.navbarService.getBoardList().subscribe(response => {
      if (response['meta']['message'] === 'SUCCESS') {
        this.boardList = response['response'];
      }
    });
  }
  openLoginDialog() {
    const dialogRef = this.dialog.open(LoginComponent, {
      height: 'auto',
      width: '600px',
      maxWidth: '90%',
      maxHeight: '90%'
    });
  }
  logout() {
    switch (this.session.sessionFrom) {
      case 'facebook':
        this.facebookService.logout().then(() => {
          this.session.removeSession().subscribe(response => {
            if (response['meta']['message'] === 'SUCCESS') {
              window.location.replace('/');
            }
          });
        }).catch(err => {
          window.location.replace('/');
        });
        break;
      default:
        this.session.removeSession().subscribe(response => {
          if (response['meta']['message'] === 'SUCCESS') {
            window.location.replace('/');
          }
        });
        break;
    }
  }
}
