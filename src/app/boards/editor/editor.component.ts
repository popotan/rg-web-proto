import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EditorService } from './editor.service';
import { SessionService } from '../../shared/session.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
  __boardInfo;
  __boardAka;
  __modTargetArticle;
  constructor(
    private translate: TranslateService,
    private session: SessionService,
    private activatedRoute: ActivatedRoute,
    private editorService: EditorService
  ) {
    this.activatedRoute.data.subscribe(data => {
      this.__boardInfo = data['boardInfo']['response'];
      if (this.activatedRoute.snapshot.params['mode'] === 'mod') {
        this.getModTargetArticle();
      }
      for (let i = 0; i < this.__boardInfo.lang_code.length; i++) {
        if (this.__boardInfo.lang_code[i].lang_code === this.translate.currentLang) {
          this.__boardAka = this.__boardInfo.lang_code[i].aka;
          break;
        }
      }
    });
  }

  ngOnInit() {
    if (!this.session.hasAuth(this.__boardInfo.write_auth)) {
      alert('Authorization is not exist.');
      window.history.go(-1);
    }
  }

  getModTargetArticle() {
    this.editorService.getArticle(
      this.activatedRoute.snapshot.params['boardName'],
      this.activatedRoute.snapshot.params['articleCid']
    ).subscribe(response => {
      this.__modTargetArticle = response['response'];
    });
  }

}
