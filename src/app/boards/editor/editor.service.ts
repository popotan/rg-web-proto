import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class EditorService {

  constructor(
    private $http: HttpClient
  ) { }

  getArticle(boardName, articleCid) {
    return this.$http.get(apiURL + `/board/${boardName}/modify/${articleCid}`)
      .pipe();
  }
}
