import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BoardsService } from './boards.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../shared/session.service';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css']
})
export class BoardsComponent implements OnInit {
  __boardInfo;
  __currentLangCode;
  __boardAka;
  __boardStyle;

  constructor(
    public session: SessionService,
    private router: Router,
    private translate: TranslateService,
    private boardsService: BoardsService,
    private activatedRoute: ActivatedRoute,
    private meta: Meta
  ) {
    this.activatedRoute.queryParams.subscribe(qparams => {
      if (Object.keys(qparams).indexOf('lang') > -1) {
        this.__currentLangCode = qparams['lang'];
      } else {
        this.__currentLangCode = translate.currentLang;
      }
    });
    this.activatedRoute.data.subscribe(data => {
      this.__boardInfo = data['boardInfo']['response'];
      this.__boardStyle = data['boardInfo']['response']['style'];
      for (const lang of this.__boardInfo['lang_code']) {
        if (lang.lang_code === this.__currentLangCode) {
          this.__boardAka = lang.aka;
        }
      }
      this.setMeta();
    });
  }

  ngOnInit() { }

  setMeta() {
    this.meta.removeTag('property="description"');
    this.meta.removeTag('property="og:title"');
    this.meta.removeTag('property="og:description"');
    this.meta.removeTag('property="og:image"');
    this.meta.addTags([
      { property: 'description', content: `ACC - ${this.__boardAka}` },
      { property: 'og:title', content: `ACC - ${this.__boardAka}` },
      { property: 'og:description', content: `ACC - ${this.__boardAka}` }
    ]);
  }

  changeLangCode($event) {
    const qparam = Object.assign({},
      this.activatedRoute.snapshot.queryParams,
      {
        lang: this.__currentLangCode
      });
    this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: qparam });
  }
}
