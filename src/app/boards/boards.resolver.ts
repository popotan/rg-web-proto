import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { BoardsService } from './boards.service';

@Injectable()
export class BoardsResolver implements Resolve<any> {
    constructor(
        private boardsService: BoardsService
    ) { }
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {
        return this.boardsService.getBoardInfo(route.params['boardName']);
    }

}
