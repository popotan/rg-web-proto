import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class BoardsService {

  constructor(
    private $http: HttpClient
  ) { }

  getBoardInfo(boardName) {
    return this.$http.get(apiURL + '/board/' + boardName + '/info')
      .pipe();
  }
}
