import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ReaderService {

  constructor(
    private $http: HttpClient
  ) { }

  getArticle(boardName, articleCid) {
    return this.$http.get(apiURL + '/board/' + boardName + '/' + articleCid)
      .pipe();
  }
  removeArticle(boardName, articleCid) {
    return this.$http.delete(apiURL + `/board/${boardName}/${articleCid}`)
      .pipe();
  }
}
