import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { ReaderService } from './reader.service';

@Injectable()
export class ReaderResolver implements Resolve<any> {
    constructor(
        private readerService: ReaderService
    ) { }
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {
        return this.readerService.getArticle(
            route.parent.params['boardName'],
            route.params['articleCid']
        );
    }

}
