import { Component, OnInit, AfterViewInit, ViewChild, Inject, PLATFORM_ID, ElementRef, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ReaderService } from './reader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { apiURL } from '../../../environments/environment.prod';
import { SessionService } from '../../shared/session.service';
import { isPlatformBrowser } from '@angular/common';
import { Meta } from '@angular/platform-browser';
import { InitParams, FacebookService } from 'ngx-facebook';
import { TruncatePipe } from '../../rg-commons/truncate.pipe';

declare var LivereTower: any;
declare var livereLib: any;
declare var jQuery: any;
declare var livereReply: any;

@Component({
  selector: 'app-reader',
  templateUrl: './reader.component.html',
  styleUrls: ['./reader.component.css'],
  providers: [ReaderService]
})
export class ReaderComponent implements OnInit, AfterViewInit {
  url_for_reply = window.location.protocol + '//' + window.location.host + window.location.pathname;
  @ViewChild('fbComments') fbComments;
  @ViewChild('fbShareButton') fbShareButton: ElementRef;
  prevCid;
  nextCid;
  article;
  apiURL = apiURL;
  constructor(
    private translate: TranslateService,
    public session: SessionService,
    private readerService: ReaderService,
    public truncatePipe: TruncatePipe,
    private facebookService: FacebookService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private meta: Meta,
    public renderer2: Renderer2,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.activatedRoute.data.subscribe(data => {
      this.prevCid = data['articleInfo']['meta']['prev_cid'];
      this.nextCid = data['articleInfo']['meta']['next_cid'];
      this.article = data['articleInfo']['response'];
      this.setMeta();
    });
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.renderer2.setAttribute(this.fbShareButton.nativeElement, 'data-href', this.url_for_share());
      this.renderer2.setAttribute(this.fbShareButton.nativeElement,
        'href', `https://www.facebook.com/sharer/sharer.php?u=${this.url_for_share()}`);
      if (isPlatformBrowser(this.platformId)) {
        const initParams: InitParams = {
          appId: '2171267379816301',
          xfbml: true,
          version: 'v2.8'
        };

        this.facebookService.init(initParams);
      }
    }
  }

  setMeta() {
    this.meta.removeTag('property="description"');
    this.meta.removeTag('property="og:title"');
    this.meta.removeTag('property="og:description"');
    this.meta.removeTag('property="og:image"');
    this.meta.addTags([
      { property: 'description', content: this.truncatePipe.transform(this.article.description, ['40', '..']) },
      { property: 'og:title', content: this.article.title },
      { property: 'og:description', content: this.truncatePipe.transform(this.article.description, ['40', '..']) },
      { property: 'og:url', content: this.url_for_reply }
    ]);
    if (this.article.file_list.length > 0 && this.isPhoto(this.article.file_list[0])) {
      this.meta.addTag({
        property: 'og:image', content: apiURL + `/board/file/${this.article.file_list[0]}`
      });
    }
  }

  url_for_share() {
    return encodeURIComponent(this.url_for_reply);
  }

  gotoPrev() {
    if (this.prevCid) {
      this.router.navigate(['/board/',
        this.activatedRoute.snapshot.parent.params['boardName'],
        'article',
        this.prevCid]);
    } else {
      this.snackBar.open('This is first page.', 'Done');
    }
  }
  gotoNext() {
    if (this.nextCid) {
      this.router.navigate(['/board/',
        this.activatedRoute.snapshot.parent.params['boardName'],
        'article',
        this.nextCid]);
    } else {
      this.snackBar.open('This is last page.', 'Done');
    }
  }
  gotoIndex() {
    this.router.navigate(['/board',
      this.activatedRoute.snapshot.parent.params['boardName']],
      {
        relativeTo: this.activatedRoute,
        queryParams: this.activatedRoute.snapshot.queryParams
      });
  }
  gotoModify() {
    this.router.navigate(['/', 'board',
      this.activatedRoute.snapshot.parent.params['boardName'],
      'mod',
      'article',
      this.activatedRoute.snapshot.params['articleCid']]);
  }
  removeArticle() {
    this.readerService.removeArticle(
      this.activatedRoute.snapshot.parent.params['boardName'],
      this.activatedRoute.snapshot.params['articleCid']
    ).subscribe(response => {
      if (response['meta']['message'] === 'SUCCESS') {
        alert('Article was just removed.');
        this.router.navigate(['/board/',
          this.activatedRoute.snapshot.parent.params['boardName']],
          { relativeTo: this.activatedRoute });
      }
    });
  }
  isPhoto(file_extension: string) {
    if (typeof file_extension === 'string') {
      const allow_extension = ['jpg', 'png', 'jpeg', 'gif'];
      if (allow_extension.indexOf(file_extension.toLowerCase()) > -1) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
