import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from '../../../environments/environment.prod';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class RgFeedEditorService {

  constructor(
    private $http: HttpClient
  ) { }

  parseOgTag(target_url) {
    return this.$http.get(apiURL + `/etc/ogp?url=${encodeURIComponent(target_url)}`)
      .pipe();
  }
  getCachedArticle(boardName, articleCid?) {
    if (articleCid) {
      return this.$http.get(apiURL + '/board/' + boardName + '/cached/' + articleCid)
        .pipe();
    } else {
      return this.$http.get(apiURL + '/board/' + boardName + '/cached')
        .pipe();
    }
  }
  getArticle(boardName, articleCid) {
    return this.$http.get(apiURL + '/board/' + boardName + '/modify/' + articleCid)
      .pipe();
  }
  postArticle(boardName, article) {
    return this.$http.post(apiURL + '/board/' + boardName, article)
      .pipe();
  }
  modArticle(boardName, articleCid, article) {
    return this.$http.put(apiURL + '/board/' + boardName + '/modify/' + articleCid, article)
      .pipe();
  }
}
