import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { RgFeedEditorService } from './rg-feed-editor.service';

@Component({
  selector: 'app-rg-feed-editor',
  templateUrl: './rg-feed-editor.component.html',
  styleUrls: ['./rg-feed-editor.component.css']
})
export class RgFeedEditorComponent implements OnInit {
  @Input('modTarget') set modTarget(value) {
    if (value) {
      this.article = value;
    }
  }
  article = {
    cid: null,
    title: '',
    description: '',
    file_list: [],
    lang_code: this.translate.currentLang,
    in_private: false,
    is_notice: false,
    popup: false
  };
  feed = {
    'title': '',
    'url': '',
    'image': '',
    'description': ''
  };
  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private rgFeedEditorService: RgFeedEditorService,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
  }

  parseOgTag($event) {
    const url = $event.target.value;
    if (url) {
      const encodedUrl = encodeURIComponent(url);
      this.rgFeedEditorService.parseOgTag(encodedUrl).subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          this.feed = response['response'];
          this.article.title = url;
        }
      });
    }
  }

  onSubmit() {
    if (!this.article.lang_code) {
      this.snackBar.open('Please select language code of the article.', 'OK.');
      return false;
    }
    this.article.title = this.feed.url;
    if (this.article.cid) {
      this.rgFeedEditorService.modArticle(
        this.activatedRoute.snapshot.params['boardName'],
        this.article.cid,
        this.article
      ).subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          this.snackBar.open('Feed is updated successfully.', 'Done');
          this.router.navigate(['/board', this.activatedRoute.snapshot.params['boardName'], 'article', response['meta']['cid']]);
        }
      }, error => {

      });
    } else {
      this.rgFeedEditorService.postArticle(
        this.activatedRoute.snapshot.params['boardName'],
        this.article
      ).subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          this.snackBar.open('Feed is updated successfully.', 'Done');
          this.router.navigate(['/board', this.activatedRoute.snapshot.params['boardName'], 'article', response['meta']['cid']]);
        }
      }, error => {

      });
    }
  }
}
