import { TestBed, inject } from '@angular/core/testing';

import { RgFeedEditorService } from './rg-feed-editor.service';

describe('RgFeedEditorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RgFeedEditorService]
    });
  });

  it('should be created', inject([RgFeedEditorService], (service: RgFeedEditorService) => {
    expect(service).toBeTruthy();
  }));
});
