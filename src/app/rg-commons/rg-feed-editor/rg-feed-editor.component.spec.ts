import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgFeedEditorComponent } from './rg-feed-editor.component';

describe('RgFeedEditorComponent', () => {
  let component: RgFeedEditorComponent;
  let fixture: ComponentFixture<RgFeedEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgFeedEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgFeedEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
