import { Component, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RgThreadService } from './rg-thread.service';
import { apiURL } from '../../../environments/environment.prod';
import { SessionService } from '../../shared/session.service';

@Component({
  selector: 'app-rg-thread',
  templateUrl: './rg-thread.component.html',
  styleUrls: ['./rg-thread.component.css']
})
export class RgThreadComponent implements OnInit {
  @ViewChild('moreButton') moreButton;
  __boardName;
  articleList = [];
  currentPage = 1;
  constructor(
    private session: SessionService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rgThreadService: RgThreadService,
    private renderer2: Renderer2
  ) {
    this.activatedRoute.queryParams.subscribe(qparams => {
      this.__boardName = this.activatedRoute.snapshot.params['boardName'];
      this.getArticleList(
        this.__boardName,
        1,
        null,
        null,
        qparams['lang']);
    });
  }

  ngOnInit() {
  }

  getArticleList(boardName, page, keyword?, keywordType?, lang?) {
    this.rgThreadService.getArticleList(boardName, page, keyword, keywordType, lang)
      .subscribe(response => {
        if (response['response'].length > 0) {
          response['response'].forEach(element => {
            this.articleList.push(element);
          });
          this.currentPage = page++;
        } else {
          this.renderer2.setStyle(this.moreButton.nativeElement, 'display', 'none');
        }
      }, error => {

      });
  }
  hasPhotoInFileList(filelist) {
    for (const info of filelist) {
      const filetype = info.filetype.toLowerCase();
      if (filetype === 'jpg' || filetype === 'png' || filetype === 'gif' || filetype === 'jpeg') {
        return true;
      }
    }
    return false;
  }
  getPhotoInFileList(filelist) {
    for (const info of filelist) {
      const filetype = info.filetype.toLowerCase();
      if (filetype === 'jpg' || filetype === 'png' || filetype === 'gif' || filetype === 'jpeg') {
        return apiURL + '/board/file/' + info.id;
      }
    }
    return 'assets/image/nophoto.png';
  }

  gotoModify(articleCid) {
    this.router.navigate(['/', 'board',
      this.activatedRoute.snapshot.params['boardName'],
      'mod',
      'article',
      articleCid]);
  }
  removeArticle(articleCid) {
    if (confirm('이 게시물을 삭제하시겠습니까?')) {
      this.rgThreadService.removeArticle(
        this.activatedRoute.snapshot.params['boardName'],
        articleCid
      ).subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          alert('Article was just removed.');
          this.router.navigate(['/board/',
            this.activatedRoute.snapshot.params['boardName']],
            { relativeTo: this.activatedRoute });
        }
      });
    }
  }
}
