import { TestBed, inject } from '@angular/core/testing';

import { RgThreadService } from './rg-thread.service';

describe('RgThreadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RgThreadService]
    });
  });

  it('should be created', inject([RgThreadService], (service: RgThreadService) => {
    expect(service).toBeTruthy();
  }));
});
