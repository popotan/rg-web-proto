import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgThreadComponent } from './rg-thread.component';

describe('RgThreadComponent', () => {
  let component: RgThreadComponent;
  let fixture: ComponentFixture<RgThreadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgThreadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgThreadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
