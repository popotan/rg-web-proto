import { Component, OnInit, ViewChild, ElementRef, Input, AfterViewInit, Renderer2, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-rg-carousel',
  templateUrl: './rg-carousel.component.html',
  styleUrls: ['./rg-carousel.component.css']
})
export class RgCarouselComponent implements OnInit {
  @ViewChild('photoList') photoListWrapper: ElementRef;
  __photoList: any[];
  @Input('photoList') set photoList(value) {
    this.__photoList = value;
  }
  currentIndex = 0;

  // tslint:disable-next-line:no-output-on-prefix
  @Output('onPhotoSelected') onPhotoSelected: EventEmitter<any> = new EventEmitter();

  constructor(
    public renderer2: Renderer2
  ) { }

  ngOnInit() {
  }
  slideTo(direction: string) {
    switch (direction) {
      case 'left':
        if (this.currentIndex > 0) {
          this.currentIndex--;
        }
        break;
      case 'right':
        if (this.currentIndex < this.__photoList.length - 1) {
          this.currentIndex++;
        }
        break;
      default:
        return false;
    }
    // .carousel ol > li {width : 303px}
    this.renderer2.setStyle(this.photoListWrapper.nativeElement, 'margin-left', (-1 * (303 * this.currentIndex)) + 'px');
  }
  selectPhoto(index) {
    this.onPhotoSelected.emit(index);
  }
}
