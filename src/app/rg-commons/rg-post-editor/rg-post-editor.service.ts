import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class RgPostEditorService {

  constructor(
    private $http: HttpClient
  ) { }

  getCachedArticle(boardName, articleCid?) {
    if (articleCid) {
      return this.$http.get(apiURL + '/board/' + boardName + '/cached/' + articleCid)
        .pipe();
    } else {
      return this.$http.get(apiURL + '/board/' + boardName + '/cached')
        .pipe();
    }
  }
  getArticle(boardName, articleCid) {
    return this.$http.get(apiURL + '/board/' + boardName + '/modify/' + articleCid)
      .pipe();
  }
  postArticle(boardName, article) {
    return this.$http.post(apiURL + '/board/' + boardName, article)
      .pipe();
  }
  modArticle(boardName, articleCid, article) {
    return this.$http.put(apiURL + '/board/' + boardName + '/modify/' + articleCid, article)
      .pipe();
  }
}
