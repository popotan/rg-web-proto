import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { RgPostEditorService } from './rg-post-editor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { SessionService } from '../../shared/session.service';

@Component({
  selector: 'app-rg-post-editor',
  templateUrl: './rg-post-editor.component.html',
  styleUrls: ['./rg-post-editor.component.css']
})
export class RgPostEditorComponent implements OnInit {
  @Input('modTarget') set modTarget(value) {
    if (value) {
      this.article = value;
    }
  }
  public editor;
  public editorOptions = {
    placeholder: 'insert content...'
  };

  article = {
    cid: null,
    title: '',
    description: '',
    file_list: [],
    lang_code: this.translate.currentLang,
    in_private: false,
    is_notice: false,
    popup: false
  };

  constructor(
    public session: SessionService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private rgPostEditorService: RgPostEditorService,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  onEditorBlured(quill) {
    // console.log('editor blur!', quill);
  }

  onEditorFocused(quill) {
    // console.log('editor focus!', quill);
  }

  onEditorCreated(quill) {
    this.editor = quill;
    // console.log('quill is ready! this is current quill instance object', quill);
  }

  onContentChanged({ quill, html, text }) {
    // console.log('quill content is changed!', quill, html, text);
  }

  ngOnInit() {
    // setTimeout(() => {
    //   this.editorContent = '<h1>content changed!</h1>';
    //   console.log('you can use the quill instance object to do something', this.editor);
    //   // this.editor.disable();
    // }, 2800);
  }

  previewArticle() {
    this.snackBar.open('Sorry, this function is working now.', 'Done');
  }

  cachingArticle() {
    this.snackBar.open('Sorry, this function is working now.', 'Done');
  }

  onSubmit() {
    if (!this.article.lang_code) {
      this.snackBar.open('Please select language code of the article.', 'OK.');
      return false;
    }
    if (this.article.cid) {
      this.rgPostEditorService.modArticle(
        this.activatedRoute.snapshot.params['boardName'],
        this.article.cid,
        this.article
      ).subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          this.snackBar.open('Article is updated successfully.', 'Done');
          this.router.navigate(['/board', this.activatedRoute.snapshot.params['boardName']]);
        }
      }, error => {

      });
    } else {
      this.rgPostEditorService.postArticle(
        this.activatedRoute.snapshot.params['boardName'],
        this.article
      ).subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          this.snackBar.open('Article is updated successfully.', 'Done');
          this.router.navigate(['/board', this.activatedRoute.snapshot.params['boardName']]);
        }
      }, error => {

      });
    }
  }

}
