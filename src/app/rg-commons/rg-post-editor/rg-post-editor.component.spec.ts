import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgPostEditorComponent } from './rg-post-editor.component';

describe('RgPostEditorComponent', () => {
  let component: RgPostEditorComponent;
  let fixture: ComponentFixture<RgPostEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgPostEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgPostEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
