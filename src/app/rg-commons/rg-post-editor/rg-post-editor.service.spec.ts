import { TestBed, inject } from '@angular/core/testing';

import { RgPostEditorService } from './rg-post-editor.service';

describe('RgPostEditorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RgPostEditorService]
    });
  });

  it('should be created', inject([RgPostEditorService], (service: RgPostEditorService) => {
    expect(service).toBeTruthy();
  }));
});
