import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCheckboxModule, MatSidenavModule, MatListModule, MatTabsModule, MatTableModule, MatPaginatorModule,
  MatFormFieldModule, MatSelectModule, MatInputModule, MatMenuModule, MatRadioModule,
  MatBadgeModule, MatChipsModule, MatCardModule, MatIconModule, MatDividerModule, MatToolbarModule, MatProgressBarModule
} from '@angular/material';

import { QuillEditorModule } from 'ngx-quill-editor';

import { RgBoardComponent } from './rg-board/rg-board.component';
import { SharedModule } from '../shared/shared.module';
import { RgPostEditorComponent } from './rg-post-editor/rg-post-editor.component';
import { FormsModule } from '@angular/forms';
import { RgFileUploaderComponent } from './rg-file-uploader/rg-file-uploader.component';
import { RouterModule } from '@angular/router';
import { FacebookCommentsCountComponent } from './facebook-comments-count/facebook-comments-count.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TruncatePipe } from './truncate.pipe';
import { PydateConvertDirective } from './pydate-convert.directive';
import { RgThreadComponent } from './rg-thread/rg-thread.component';
import { RgFeedComponent } from './rg-feed-list/rg-feed/rg-feed.component';
import { RgFeedEditorComponent } from './rg-feed-editor/rg-feed-editor.component';
import { RgFeedListComponent } from './rg-feed-list/rg-feed-list.component';
import { RgAlbumListComponent } from './rg-album-list/rg-album-list.component';
import { RgAlbumComponent } from './rg-album-list/rg-album/rg-album.component';
import { RgAlbumEditorComponent } from './rg-album-editor/rg-album-editor.component';
import { RgCarouselComponent } from './rg-carousel/rg-carousel.component';
import { RgAlbumExpandedViewerComponent } from './rg-album-list/rg-album/rg-album-expanded-viewer/rg-album-expanded-viewer.component';
import { UpRiseMotionDirective } from './up-rise-motion.directive';
import { SafeHtmlPipe } from './safe-html.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    HttpModule,
    HttpClientModule,
    SharedModule,
    BrowserAnimationsModule,
    MatButtonModule, MatCheckboxModule, MatSidenavModule, MatListModule, MatTabsModule, MatTableModule, MatPaginatorModule,
    MatFormFieldModule, MatSelectModule, MatInputModule, MatMenuModule, MatRadioModule,
    MatBadgeModule, MatChipsModule, MatCardModule, MatIconModule, MatDividerModule, MatToolbarModule,
    MatProgressBarModule,
    QuillEditorModule
  ],
  exports: [
    RgBoardComponent,
    RgPostEditorComponent,
    RgFileUploaderComponent,
    FacebookCommentsCountComponent,
    PydateConvertDirective,
    TruncatePipe,
    RgThreadComponent,
    RgFeedComponent,
    RgFeedEditorComponent,
    RgFeedListComponent,
    RgAlbumComponent,
    RgAlbumListComponent,
    RgAlbumEditorComponent,
    RgCarouselComponent,
    RgAlbumExpandedViewerComponent,
    UpRiseMotionDirective,
    SafeHtmlPipe
  ],
  declarations: [
    RgBoardComponent,
    RgPostEditorComponent,
    RgFileUploaderComponent,
    FacebookCommentsCountComponent,
    TruncatePipe,
    PydateConvertDirective,
    RgThreadComponent,
    RgFeedComponent,
    RgFeedEditorComponent,
    RgFeedListComponent,
    RgAlbumListComponent,
    RgAlbumComponent,
    RgAlbumEditorComponent,
    RgCarouselComponent,
    RgAlbumExpandedViewerComponent,
    UpRiseMotionDirective,
    SafeHtmlPipe]
})
export class RgCommonsModule { }
