import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgBoardComponent } from './rg-board.component';

describe('RgBoardComponent', () => {
  let component: RgBoardComponent;
  let fixture: ComponentFixture<RgBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
