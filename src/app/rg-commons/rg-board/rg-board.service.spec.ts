import { TestBed, inject } from '@angular/core/testing';

import { RgBoardService } from './rg-board.service';

describe('RgBoardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RgBoardService]
    });
  });

  it('should be created', inject([RgBoardService], (service: RgBoardService) => {
    expect(service).toBeTruthy();
  }));
});
