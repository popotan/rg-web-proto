import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from '../../../environments/environment.prod';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class RgBoardService {

  constructor(
    private $http: HttpClient,
    private translate: TranslateService
  ) { }

  getBoardInfo(boardName) {
    return this.$http.get(apiURL + '/board/' + boardName + '/info')
      .pipe();
  }

  getArticleList(boardName, page, keyword?, keywordType?, lang?) {
    if (!lang) {
      lang = this.translate.currentLang;
    }
    let keywordParams = '';
    if (keyword && keywordType) {
      keywordParams += '&keyword=' + keyword + '&keyword_type=' + keywordType;
    }
    return this.$http.get(apiURL + '/board/' + boardName + '?page=' + page + '&lang=' + lang + keywordParams)
      .pipe();
  }

  getPopupArticleList(boardName, lang?) {
    if (!lang) {
      lang = this.translate.currentLang;
    }
    return this.$http.get(`${apiURL}/board/${boardName}/popup?lang=${lang}`).pipe();
  }
}
