import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RgBoardService } from './rg-board.service';

export interface ArticleHeader {
  cid: number;
  title: string;
  username: string;
  access_count: number;
  reg_date: string;
  reply_length: number;
}

const ARTICLE_DATA_FOR_TEST: ArticleHeader[] = [
  { cid: 1, title: 'test 테스트입니다', username: '이가람', reg_date: '2018-03-02 13:00:01', access_count: 12345, reply_length: 0 },
  { cid: 2, title: 'test 테스트입니다', username: '이가람', reg_date: '2018-03-02 13:00:01', access_count: 1235, reply_length: 12 },
  { cid: 3, title: 'test 테스트입니다', username: '이가람', reg_date: '2018-03-02 13:00:01', access_count: 123, reply_length: 2019 }
];

@Component({
  selector: 'app-rg-board',
  templateUrl: './rg-board.component.html',
  styleUrls: ['./rg-board.component.css'],
  providers: [RgBoardService]
})
export class RgBoardComponent implements OnInit {
  displayedColumns: string[] = ['cid', 'title', 'guest_name', 'reg_date', 'view_count'];
  dataSource = new MatTableDataSource<ArticleHeader>(ARTICLE_DATA_FOR_TEST);
  selection = new SelectionModel<ArticleHeader>(true, []);

  keywordType = 'article';
  keyword = '';

  __boardName;
  presentHost = window.location.protocol + '//' + window.location.host;

  articleList = [];
  articleTotalCount = 0;

  constructor(
    private translate: TranslateService,
    public activatedRoute: ActivatedRoute,
    private router: Router,
    private rgBoardService: RgBoardService
  ) {
    this.activatedRoute.queryParams.subscribe(qparams => {
      this.getArticleList(
        this.__boardName,
        (Object.keys(qparams).indexOf('page') === -1) ? 1 : qparams['page'],
        qparams['keyword'],
        qparams['keyword_type'],
        qparams['lang']);
    });
    this.activatedRoute.params.subscribe(params => {
      this.__boardName = this.activatedRoute.snapshot.params['boardName'];
      if (params['boardName']) {
        this.getArticleList(
          this.__boardName,
          // tslint:disable-next-line:max-line-length
          (Object.keys(this.activatedRoute.snapshot.queryParams).indexOf('page') === -1) ? 1 : this.activatedRoute.snapshot.queryParams['page'],
          this.activatedRoute.snapshot.queryParams['keyword'],
          this.activatedRoute.snapshot.queryParams['keyword_type'],
          this.activatedRoute.snapshot.queryParams['lang']);
      }
    });
  }

  ngOnInit() {
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  getArticleList(boardName, page, keyword?, keywordType?, lang?) {
    this.rgBoardService.getArticleList(boardName, page, keyword, keywordType, lang)
      .subscribe(response => {
        this.articleTotalCount = response['meta']['total_count'];
        this.articleList = response['response'];
      }, error => {

      });
  }

  onPageChange($event) {
    const presentParams = this.activatedRoute.snapshot.queryParams;
    const qparam = Object.assign({}, presentParams, { page: $event.pageIndex + 1 });
    this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: qparam });
  }

  searchKeyword() {
    const presentParams = this.activatedRoute.snapshot.queryParams;
    const qparam = Object.assign({}, presentParams, { page: 1, keyword: this.keyword, keyword_type: this.keywordType });
    this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: qparam });
  }

  clearKeyword() {
    this.router.navigate([], { relativeTo: this.activatedRoute });
  }

  goto(articleCid) {
    this.activatedRoute.params.subscribe(params => {
      const turl = '/board/' + params['boardName'] + '/article/' + articleCid;
      const presentParams = this.activatedRoute.snapshot.queryParams;
      this.router.navigateByUrl(turl, { relativeTo: this.activatedRoute, queryParams: presentParams });
    });
  }

}
