import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgAlbumEditorComponent } from './rg-album-editor.component';

describe('RgAlbumEditorComponent', () => {
  let component: RgAlbumEditorComponent;
  let fixture: ComponentFixture<RgAlbumEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgAlbumEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgAlbumEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
