import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RgAlbumListService } from './rg-album-list.service';

@Component({
  selector: 'app-rg-album-list',
  templateUrl: './rg-album-list.component.html',
  styleUrls: ['./rg-album-list.component.css']
})
export class RgAlbumListComponent implements OnInit {
  __albumName;
  articleList = [];
  articleTotalCount = 0;
  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rgAlbumListService: RgAlbumListService
  ) {
    this.activatedRoute.queryParams.subscribe(qparams => {
      this.getArticleList(
        this.activatedRoute.snapshot.params['boardName'],
        (Object.keys(qparams).indexOf('page') === -1) ? 1 : qparams['page'],
        qparams['keyword'],
        qparams['keyword_type'],
        qparams['lang']
      );
    });
  }

  ngOnInit() {
  }

  getArticleList(boardName, page, keyword?, keywordType?, lang?) {
    console.log('rg-album-loaded');
    this.rgAlbumListService.getArticleList(boardName, page, keyword, keywordType, lang)
      .subscribe(response => {
        this.articleTotalCount = response['meta']['total_count'];
        this.articleList = response['response'];
      }, error => {

      });
  }
  onPageChange($event) {
    const presentParams = this.activatedRoute.snapshot.queryParams;
    const qparam = Object.assign({}, presentParams, { page: $event.pageIndex + 1 });
    this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: qparam });
  }
}
