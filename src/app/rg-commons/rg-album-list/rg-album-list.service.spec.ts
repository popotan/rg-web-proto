import { TestBed, inject } from '@angular/core/testing';

import { RgAlbumListService } from './rg-album-list.service';

describe('RgAlbumListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RgAlbumListService]
    });
  });

  it('should be created', inject([RgAlbumListService], (service: RgAlbumListService) => {
    expect(service).toBeTruthy();
  }));
});
