import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgAlbumListComponent } from './rg-album-list.component';

describe('RgAlbumListComponent', () => {
  let component: RgAlbumListComponent;
  let fixture: ComponentFixture<RgAlbumListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgAlbumListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgAlbumListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
