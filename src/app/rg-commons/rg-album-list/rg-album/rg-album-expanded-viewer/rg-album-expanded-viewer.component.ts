import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-rg-album-expanded-viewer',
  templateUrl: './rg-album-expanded-viewer.component.html',
  styleUrls: ['./rg-album-expanded-viewer.component.css']
})
export class RgAlbumExpandedViewerComponent implements OnInit {
  ref;
  photoList: any[];
  currentIndex = 0;

  constructor() { }

  ngOnInit() {
  }

  slideTo(direction: string) {
    switch (direction) {
      case 'left':
        if (this.currentIndex > 0) {
          this.currentIndex--;
        }
        break;
      case 'right':
        if (this.currentIndex < this.photoList.length - 1) {
          this.currentIndex++;
        }
        break;
      default:
        return false;
    }
  }
  close() {
    this.ref.destroy();
  }
}
