import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgAlbumExpandedViewerComponent } from './rg-album-expanded-viewer.component';

describe('RgAlbumExpandedViewerComponent', () => {
  let component: RgAlbumExpandedViewerComponent;
  let fixture: ComponentFixture<RgAlbumExpandedViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgAlbumExpandedViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgAlbumExpandedViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
