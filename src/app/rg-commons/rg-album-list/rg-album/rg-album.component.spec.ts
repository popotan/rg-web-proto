import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgAlbumComponent } from './rg-album.component';

describe('RgAlbumComponent', () => {
  let component: RgAlbumComponent;
  let fixture: ComponentFixture<RgAlbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgAlbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
