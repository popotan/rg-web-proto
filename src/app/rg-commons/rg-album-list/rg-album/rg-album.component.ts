import { Component, OnInit, Input, ViewContainerRef, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { apiURL } from '../../../../environments/environment.prod';
import { RgAlbumExpandedViewerComponent } from './rg-album-expanded-viewer/rg-album-expanded-viewer.component';
import { RgAlbumListService } from '../rg-album-list.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '../../../shared/session.service';

@Component({
  selector: 'app-rg-album',
  templateUrl: './rg-album.component.html',
  styleUrls: ['./rg-album.component.css'],
  entryComponents: [RgAlbumExpandedViewerComponent]
})
export class RgAlbumComponent implements OnInit {
  photoList = [];
  __article;
  isShowing = false;
  @Input('mode') mode = 'album';
  @Input('expand') expand = true;
  @Input('article') set article(value: object) {
    this.__article = value;
    this.photoList = [];
    if (value.hasOwnProperty('file_list')) {
      for (let i = 0; i < value['file_list'].length; i++) {
        if (this.isPhoto(value['file_list'][i]['filetype'])) {
          this.photoList.push({ src: `${apiURL}/board/file/${value['file_list'][i]['id']}` });
        }
      }
    }
  }
  @ViewChild('rgAlbumExpandedViewContainer', { read: ViewContainerRef }) rgAlbumExpandedViewerContainer;
  createdComponent;
  constructor(
    private rgAlbumListService: RgAlbumListService,
    public session: SessionService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private cfr: ComponentFactoryResolver
  ) { }

  ngOnInit() {
  }

  isPhoto(file_extension: string) {
    if (typeof file_extension === 'string') {
      const allow_extension = ['jpg', 'png', 'jpeg', 'gif'];
      if (allow_extension.indexOf(file_extension.toLowerCase()) > -1) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  expandView($event: number) {
    if (this.expand) {
      this.rgAlbumExpandedViewerContainer.clear();
      const factory = this.cfr.resolveComponentFactory(RgAlbumExpandedViewerComponent);
      this.createdComponent = this.rgAlbumExpandedViewerContainer.createComponent(factory);
      this.createdComponent.instance.ref = this.createdComponent;
      this.createdComponent.instance.currentIndex = $event;
      this.createdComponent.instance.photoList = this.photoList;
    }
  }
  gotoModify() {
    this.router.navigate(['/', 'board',
      this.activatedRoute.snapshot.params['boardName'],
      'mod',
      'article',
      this.__article.cid]);
  }
  removeArticle() {
    this.rgAlbumListService.removeArticle(
      this.activatedRoute.snapshot.params['boardName'],
      this.__article.cid
    ).subscribe(response => {
      if (response['meta']['message'] === 'SUCCESS') {
        alert('Article was just removed.');
        this.router.navigate(['/board/',
          this.activatedRoute.snapshot.params['boardName']],
          { relativeTo: this.activatedRoute });
      }
    });
  }
}
