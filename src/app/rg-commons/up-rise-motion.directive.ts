import { Directive, ElementRef, Renderer2, AfterViewInit, HostListener, OnInit } from '@angular/core';

@Directive({
  selector: '[appUpRiseMotion]'
})
export class UpRiseMotionDirective implements OnInit, AfterViewInit {
  posTop;
  constructor(
    public el: ElementRef,
    public renderer2: Renderer2
  ) { }

  ngOnInit() {
    // this.posTop = this.el.nativeElement.offsetTop;
  }

  ngAfterViewInit() {
    this.posTop = this.el.nativeElement.getBoundingClientRect().top;
    this.renderer2.setStyle(this.el.nativeElement, 'transition', '1.375s');
    if (((window.scrollY || window.pageYOffset) + (window.innerHeight || document.body.clientHeight)) * 0.95 > this.posTop) {
      this.renderer2.setStyle(this.el.nativeElement, 'opacity', '0');
    } else {
      this.renderer2.setStyle(this.el.nativeElement, 'opacity', '1');
    }
  }

  @HostListener('document:scroll', ['$event'])
  onWindowScroll($event) {
    if (((window.scrollY || window.pageYOffset) + (window.innerHeight || document.body.clientHeight)) * 0.95 > this.posTop) {
      this.renderer2.setStyle(this.el.nativeElement, 'opacity', '1');
      this.renderer2.setStyle(this.el.nativeElement, 'transform', 'translateY(0%)');
    } else {
      this.renderer2.setStyle(this.el.nativeElement, 'opacity', '0');
      this.renderer2.setStyle(this.el.nativeElement, 'transform', 'translateY(30%)');
    }
  }
}
