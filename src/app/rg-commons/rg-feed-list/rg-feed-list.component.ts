import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RgFeedListService } from './rg-feed-list.service';
import { apiURL } from '../../../environments/environment.prod';

@Component({
  selector: 'app-rg-feed-list',
  templateUrl: './rg-feed-list.component.html',
  styleUrls: ['./rg-feed-list.component.css']
})
export class RgFeedListComponent implements OnInit {
  boardType;
  __feedName;
  presentHost = window.location.protocol + '//' + window.location.host;

  articleList = [];
  articleTotalCount = 0;
  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rgFeedService: RgFeedListService
  ) {
    this.activatedRoute.params.subscribe(params => {
      if (params['boardName'] === 'photo') {
        this.boardType = 'photo';
      } else {
        this.boardType = 'feed';
      }
    });
    this.activatedRoute.queryParams.subscribe(qparams => {
      if (Object.keys(qparams).indexOf('page') === -1) {
        const presentParams = this.activatedRoute.snapshot.queryParams;
        const qparam = Object.assign({}, presentParams, { page: 1 });
        this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: qparam });
      } else {
        this.__feedName = this.activatedRoute.snapshot.params['boardName'];

        this.getArticleList(
          this.__feedName,
          qparams['page'],
          qparams['keyword'],
          qparams['keyword_type'],
          qparams['lang']);
      }
    });
  }

  ngOnInit() {
  }

  getArticleList(boardName, page, keyword?, keywordType?, lang?) {
    this.rgFeedService.getArticleList(boardName, page, keyword, keywordType, lang)
      .subscribe(response => {
        this.articleTotalCount = response['meta']['total_count'];
        this.articleList = response['response'];
      }, error => {

      });
  }

  onPageChange($event) {
    const presentParams = this.activatedRoute.snapshot.queryParams;
    const qparam = Object.assign({}, presentParams, { page: $event.pageIndex + 1 });
    this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: qparam });
  }

  goto(articleCid) {
    this.activatedRoute.params.subscribe(params => {
      const turl = '/board/' + params['boardName'] + '/article/' + articleCid;
      const presentParams = this.activatedRoute.snapshot.queryParams;
      this.router.navigateByUrl(turl, { relativeTo: this.activatedRoute, queryParams: presentParams });
    });
  }

  getPhotoInFileList(filelist) {
    for (const info of filelist) {
      const filetype = info.filetype.toLowerCase();
      if (filetype === 'jpg' || filetype === 'png' || filetype === 'gif' || filetype === 'jpeg') {
        return apiURL + '/board/file/' + info.id;
      }
    }
    return '';
  }

}
