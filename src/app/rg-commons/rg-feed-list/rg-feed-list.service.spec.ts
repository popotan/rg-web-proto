import { TestBed, inject } from '@angular/core/testing';

import { RgFeedListService } from './rg-feed-list.service';

describe('RgFeedListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RgFeedListService]
    });
  });

  it('should be created', inject([RgFeedListService], (service: RgFeedListService) => {
    expect(service).toBeTruthy();
  }));
});
