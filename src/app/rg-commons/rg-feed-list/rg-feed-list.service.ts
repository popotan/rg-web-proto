import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class RgFeedListService {

  constructor(
    private $http: HttpClient,
    private translate: TranslateService
  ) { }

  getArticleList(boardName, page, keyword?, keywordType?, lang?) {
    if (!lang) {
      lang = this.translate.currentLang;
    }
    let keywordParams = '';
    if (keyword && keywordType) {
      keywordParams += '&keyword=' + keyword + '&keyword_type=' + keywordType;
    }
    return this.$http.get(apiURL + '/board/' + boardName + '?page=' + page + '&lang=' + lang + keywordParams)
      .pipe();
  }
}
