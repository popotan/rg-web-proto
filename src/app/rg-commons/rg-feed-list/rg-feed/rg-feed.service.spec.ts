import { TestBed, inject } from '@angular/core/testing';

import { RgFeedService } from './rg-feed.service';

describe('RgFeedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RgFeedService]
    });
  });

  it('should be created', inject([RgFeedService], (service: RgFeedService) => {
    expect(service).toBeTruthy();
  }));
});
