import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { apiURL } from '../../../../environments/environment.prod';
import { RgFeedService } from './rg-feed.service';
import { SessionService } from '../../../shared/session.service';

@Component({
  selector: 'app-rg-feed',
  templateUrl: './rg-feed.component.html',
  styleUrls: ['./rg-feed.component.css']
})
export class RgFeedComponent implements OnInit {
  __url;
  @Input('url') set url(value) {
    if (value) {
      this.__url = value;
      this.parseOgTag(value);
    }
  }
  __article;
  @Input('article') set article(value) {
    this.__article = value;
  }
  feed = {
    'title': '',
    'url': '',
    'image': '',
    'description': ''
  };
  @Input('mode') mode?;
  constructor(
    private rgFeedService: RgFeedService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public session: SessionService
  ) { }
  ngOnInit() { }
  parseOgTag(url) {
    if (url) {
      const encodedUrl = encodeURIComponent(url);
      this.rgFeedService.parseOgTag(encodedUrl).subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          this.feed = response['response'];
        }
      });
    }
  }
  getPhotoInFileList(filelist) {
    for (const info of filelist) {
      const filetype = info.filetype.toLowerCase();
      if (filetype === 'jpg' || filetype === 'png' || filetype === 'gif' || filetype === 'jpeg') {
        return apiURL + '/board/file/' + info.id;
      }
    }
    return 'assets/image/nophoto.png';
  }
  goto() {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('boardName')) {
        const turl = '/board/' + params['boardName'] + '/article/' + this.__article.cid;
        const presentParams = this.activatedRoute.snapshot.queryParams;
        this.router.navigateByUrl(turl, { relativeTo: this.activatedRoute, queryParams: presentParams });
      }
    });
  }
  removeArticle() {
    this.rgFeedService.removeArticle(
      this.activatedRoute.snapshot.params['boardName'],
      this.__article.cid
    ).subscribe(response => {
      if (response['meta']['message'] === 'SUCCESS') {
        alert('Article was just removed.');
        this.router.navigate(['/board/',
          this.activatedRoute.snapshot.parent.params['boardName']],
          { relativeTo: this.activatedRoute });
      }
    });
  }
  share() {
    alert('sorry, function to share article is implementing.');
  }
}
