import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiURL } from '../../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class RgFeedService {

  constructor(
    private $http: HttpClient
  ) { }
  parseOgTag(target_url) {
    return this.$http.get(apiURL + `/etc/ogp?url=${encodeURIComponent(target_url)}`)
      .pipe();
  }
  removeArticle(boardName, articleCid) {
    return this.$http.delete(apiURL + `/board/${boardName}/${articleCid}`)
      .pipe();
  }
}
