import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgFeedComponent } from './rg-feed.component';

describe('RgFeedComponent', () => {
  let component: RgFeedComponent;
  let fixture: ComponentFixture<RgFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
