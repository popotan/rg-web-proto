import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgFeedListComponent } from './rg-feed-list.component';

describe('RgFeedListComponent', () => {
  let component: RgFeedListComponent;
  let fixture: ComponentFixture<RgFeedListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgFeedListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgFeedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
