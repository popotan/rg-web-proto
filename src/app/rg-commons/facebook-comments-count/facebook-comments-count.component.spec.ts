import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacebookCommentsCountComponent } from './facebook-comments-count.component';

describe('FacebookCommentsCountComponent', () => {
  let component: FacebookCommentsCountComponent;
  let fixture: ComponentFixture<FacebookCommentsCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacebookCommentsCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookCommentsCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
