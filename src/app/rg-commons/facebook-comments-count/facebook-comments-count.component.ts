import { Component, OnInit, Input, AfterViewInit } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { map, filter, catchError, mergeMap } from 'rxjs/operators';
import { FacebookService, AuthResponse } from 'ngx-facebook';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-facebook-comments-count',
  templateUrl: './facebook-comments-count.component.html',
  styleUrls: ['./facebook-comments-count.component.css']
})
export class FacebookCommentsCountComponent implements OnInit, AfterViewInit {
  @Input('targetUrl') set targetUrl(url) {

    let f_api_url = 'https://graph.facebook.com/v2.8/?fields=share{comment_count}&id=';
    f_api_url += encodeURIComponent(url);

    this.$http.get(f_api_url)
      .pipe()
      .subscribe(result => {
        this.result = result['share']['comment_count'];
      }, error => {
        this.result = '?';
      });
  }
  result;
  constructor(
    private $http: HttpClient,
    private fb: FacebookService
  ) { }

  ngOnInit() {

  }
  ngAfterViewInit() {

  }

}
