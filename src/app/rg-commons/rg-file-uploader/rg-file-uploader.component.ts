import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { RgFileUploaderService } from './rg-file-uploader.service';
import { apiURL } from '../../../environments/environment.prod';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-rg-file-uploader',
  templateUrl: './rg-file-uploader.component.html',
  styleUrls: ['./rg-file-uploader.component.css']
})
export class RgFileUploaderComponent implements OnInit {
  // tslint:disable-next-line:no-output-on-prefix
  @Output('onFileUploadComplete') onFileUploadComplete: EventEmitter<any> = new EventEmitter();
  @ViewChild('uploadedFiles') uploadedFiles;
  @Input('uploadedFileList') uploadedFileList: any[];
  selectedFilesIndex = [];
  isUploading = false;
  progress = 0;

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    public rgFileUploadService: RgFileUploaderService
  ) {
    this.rgFileUploadService.progress$.subscribe(value => {
      this.progress = value;
    });
  }

  ngOnInit() {
  }

  removeFiles() {
    let removedLength = 0;
    while (this.selectedFilesIndex.length > 0) {
      this.uploadedFileList.splice(this.selectedFilesIndex.pop() - removedLength, 1);
      removedLength++;
    }
    this.onFileUploadComplete.emit(this.uploadedFileList);
  }

  uploadFiles($event) {
    this.isUploading = true;
    this.rgFileUploadService.uploadFile(
      apiURL + '/board/' + this.activatedRoute.snapshot.params['boardName'] + '/file',
      $event.target.files).then(response => {
        response['response'].forEach(fileInfo => {
          this.uploadedFileList.push(fileInfo);
        });
        this.isUploading = false;
      });
    this.onFileUploadComplete.emit(this.uploadedFileList);
  }
}
