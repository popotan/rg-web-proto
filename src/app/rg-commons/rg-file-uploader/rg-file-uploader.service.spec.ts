import { TestBed, inject } from '@angular/core/testing';

import { RgFileUploaderService } from './rg-file-uploader.service';

describe('RgFileUploaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RgFileUploaderService]
    });
  });

  it('should be created', inject([RgFileUploaderService], (service: RgFileUploaderService) => {
    expect(service).toBeTruthy();
  }));
});
