import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgCarouselComponent } from './rg-carousel.component';

describe('RgCarouselComponent', () => {
  let component: RgCarouselComponent;
  let fixture: ComponentFixture<RgCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
