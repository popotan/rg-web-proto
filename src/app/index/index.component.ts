import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, OnDestroy, HostListener, Inject, PLATFORM_ID } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { RgBoardService } from '../rg-commons/rg-board/rg-board.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { apiURL } from '../../environments/environment.prod';
import { isPlatformBrowser } from '@angular/common';
import { Meta } from '@angular/platform-browser';
import { RgCarousel } from '../composer/rg-commons/rg-carousel/rg-carousel';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, AfterViewInit, OnDestroy {
  headerCarousel: RgCarousel;
  map: any;
  showingStateListOfProjectArticle = [true, false, false, false, false];
  accnow = [];
  recentNews = [];
  recentPhoto = [];
  ref_name_list = ['ko', 'jp', 'cn', 'my', 'th', 'vn', 'ph', 'sg', 'id', 'kh', 'mm', 'la', 'kp', 'apecaiib', 'asiabasic', 'asiacommon', 'oda'];
  ref_board_info = {};
  recentReference = {};
  recentNotice = [];
  popupArticles = [];
  recentNoticeIndex = 0;
  apiURL = apiURL;

  constructor(
    private translate: TranslateService,
    private rgBoardService: RgBoardService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private meta: Meta,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.meta.addTags([
      { property: 'og:title', content: 'Asia Cooperation Center - (사)아시아교류협력센터, ACC' },
      { property: 'og:description', content: '협력과 상생의 아시아, 함께 이루는 공동선' },
      { property: 'og:keyword', content: 'ACC,아시아교류협력센터,이동검진,산업기술표준화,산업인력자원개발,친환경에너지,신재생에너지,기술교류,문화교류,유학생네트워크' }
    ]);
  }

  ngOnInit() {
    this.headerCarousel = new RgCarousel();
    this.headerCarousel.cssWidth = '100%';
    this.headerCarousel.cssHeight = '100%';
    this.headerCarousel.slideTimer = 10000;

    this.getAccNow();
    this.getRecentNews();
    this.getPhoto();
    // this.getRecentNotice();
    this.getReferences();
    this.getPopupArticleList();
  }

  ngAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      if (window.innerWidth >= 960) {
        // this.initFullpage();
      }
    }
  }

  ngOnDestroy() {
  }

  showProjectArticle(index) {
    for (let i = 0; i < this.showingStateListOfProjectArticle.length; i++) {
      if (index !== i) {
        this.showingStateListOfProjectArticle[i] = false;
      } else {
        this.showingStateListOfProjectArticle[i] = true;
      }
    }
  }

  gotoContactUsPage(email) {
    this.router.navigate(['/', 'article', 'contact-us'], { queryParams: { email: email } });
  }

  getRecentNotice() {
    this.rgBoardService.getArticleList('notice', 1, undefined, undefined, 'ko').subscribe(response => {
      if (response['meta']['message'] === 'SUCCESS') {
        this.recentNotice = response['response'];
      }
    });
  }
  selectRecentNotice(index) {
    if (window.innerWidth > 960) {
      this.recentNoticeIndex = index;
    } else {
      window.open(`http://${window.location.host}/board/notice/article/${this.recentNotice[index].cid}`, '_blank');
    }
  }

  getRecentNews() {
    this.rgBoardService.getArticleList('news', 1, undefined, undefined, 'ko')
      .subscribe(response => {
        this.recentNews = response['response'];
      }, error => {

      });
  }

  getAccNow() {
    this.rgBoardService.getArticleList('accnow', 1, undefined, undefined, 'ko')
      .subscribe(response => {
        this.accnow = response['response'];
      });
  }

  isPhoto(file_extension: string) {
    if (typeof file_extension === 'string') {
      const allow_extension = ['jpg', 'png', 'jpeg', 'gif'];
      if (allow_extension.indexOf(file_extension.toLowerCase()) > -1) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  getPhoto() {
    this.rgBoardService.getArticleList('photo', 1, undefined, undefined, 'ko')
      .subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          this.recentPhoto = response['response'];
        }
      });
  }

  goto(boardName, articleCid) {
    const turl = '/board/' + boardName + '/article/' + articleCid;
    const presentParams = this.activatedRoute.snapshot.queryParams;
    this.router.navigateByUrl(turl, { relativeTo: this.activatedRoute, queryParams: presentParams });
  }

  hasPhotoInFileList(filelist) {
    for (const info of filelist) {
      const filetype = info.filetype.toLowerCase();
      if (filetype === 'jpg' || filetype === 'png' || filetype === 'gif' || filetype === 'jpeg') {
        return true;
      }
    }
    return false;
  }

  getPhotoInFileList(filelist) {
    for (const info of filelist) {
      const filetype = info.filetype.toLowerCase();
      if (filetype === 'jpg' || filetype === 'png' || filetype === 'gif' || filetype === 'jpeg') {
        return apiURL + '/board/file/' + info.id;
      }
    }
    return 'assets/image/nophoto.png';
  }

  getReferences() {
    for (let i = 0; i < this.ref_name_list.length; i++) {
      this.rgBoardService.getBoardInfo(`ref_${this.ref_name_list[i]}`).subscribe(response => {
        this.ref_board_info[`ref_${this.ref_name_list[i]}`] = response['response'];
        this.rgBoardService.getArticleList(`ref_${this.ref_name_list[i]}`, 1, undefined, undefined, 'ko').subscribe(response2 => {
          if (response['meta']['message'] === 'SUCCESS') {
            this.recentReference[`ref_${this.ref_name_list[i]}`] = response2['response'];
          }
        });
      });
    }
  }

  getPopupArticleList() {
    this.rgBoardService.getPopupArticleList('notice').subscribe(response => {
      this.popupArticles = response['response'];
    });
  }
}
