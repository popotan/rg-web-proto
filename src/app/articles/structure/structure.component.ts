import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-structure',
  templateUrl: './structure.component.html',
  styleUrls: ['./structure.component.css']
})
export class StructureComponent implements OnInit {
  currentLang;
  constructor(
    private translate: TranslateService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
  }
  ngOnInit() {
  this.currentLang = this.translate.currentLang;
    if (isPlatformBrowser(this.platformId)) {
      this.translate.onLangChange.subscribe($event => {
        this.currentLang = $event.lang;
      });
    }
  }
}
