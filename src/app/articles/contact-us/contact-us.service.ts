import { Injectable } from '@angular/core';
import { apiURL } from '../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactUsService {

  constructor(
    private $http: HttpClient
  ) { }

  postArticle(article) {
    return this.$http.post(apiURL + `/etc/contact_us`, article)
      .pipe();
  }
}
