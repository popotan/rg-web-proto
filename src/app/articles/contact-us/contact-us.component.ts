import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ErrorStateMatcher, MatSnackBar } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ContactUsService } from './contact-us.service';
import { ActivatedRoute } from '@angular/router';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  titleFormControl = new FormControl('', [
    Validators.required
  ]);
  descriptionFormControl = new FormControl('', [
    Validators.required
  ]);

  matcher = new MyErrorStateMatcher();
  constructor(
    private activatedRoute: ActivatedRoute,
    private contactUsService: ContactUsService,
    private snackBar: MatSnackBar
  ) {
    this.activatedRoute.queryParams.subscribe(qparams => {
      if (Object.keys(qparams).indexOf('email') > -1) {
        this.emailFormControl.setValue(qparams['email']);
        this.titleFormControl.setValue('가입문의(to request join)');
      }
    });
  }

  ngOnInit() {
  }

  doSubmit($event) {
    $event.target.disabled = true;
    if (!this.emailFormControl.invalid &&
      !this.titleFormControl.invalid &&
      !this.descriptionFormControl.invalid) {
      this.contactUsService.postArticle({
        'email': this.emailFormControl.value,
        'title': this.titleFormControl.value,
        'description': this.descriptionFormControl.value
      }).subscribe(response => {
        if (response['meta']['message'] === 'SUCCESS') {
          this.snackBar.open('The form was submitted successfully.', 'Done');
          this.emailFormControl.setValue('');
        }
      }, error => {
        this.snackBar.open('something wrong, please try again.', 'Done');
        $event.target.disabled = false;
      });
    } else {
      this.snackBar.open('Submitted form is invalid.', 'Done');
      $event.target.disabled = false;
    }
  }
}
