import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeWithComponent } from './be-with.component';

describe('BeWithComponent', () => {
  let component: BeWithComponent;
  let fixture: ComponentFixture<BeWithComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeWithComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeWithComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
