import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoEmailComponent } from './no-email.component';

describe('NoEmailComponent', () => {
  let component: NoEmailComponent;
  let fixture: ComponentFixture<NoEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
