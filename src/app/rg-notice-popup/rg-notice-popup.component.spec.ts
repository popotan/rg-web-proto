import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RgNoticePopupComponent } from './rg-notice-popup.component';

describe('RgNoticePopupComponent', () => {
  let component: RgNoticePopupComponent;
  let fixture: ComponentFixture<RgNoticePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RgNoticePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RgNoticePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
