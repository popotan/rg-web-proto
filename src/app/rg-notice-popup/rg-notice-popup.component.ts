import { Component, OnInit, Input, ElementRef, Renderer2, Inject, PLATFORM_ID, AfterViewInit } from '@angular/core';
import { apiURL } from '../../environments/environment.prod';
import { isPlatformBrowser } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-rg-notice-popup',
  templateUrl: './rg-notice-popup.component.html',
  styleUrls: ['./rg-notice-popup.component.css']
})
export class RgNoticePopupComponent implements OnInit, AfterViewInit {
  apiURL = apiURL;
  @Input('article') article;
  constructor(
    public el: ElementRef,
    public renderer2: Renderer2,
    @Inject(PLATFORM_ID) private platformId: Object
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      const date = localStorage.getItem(`notice_${this.article.cid}`);
      if (date) {
        const dateArr = date.split('-');
        const today = new Date();
        if (today.getFullYear().toString() === dateArr[0] &&
          today.getMonth().toString() === dateArr[1] &&
          today.getDate().toString() === dateArr[2]) {
          this.renderer2.setStyle(this.el.nativeElement, 'display', 'none');
        }
      }
    }
  }

  isPhoto(file_extension: string) {
    if (typeof file_extension === 'string') {
      const allow_extension = ['jpg', 'png', 'jpeg', 'gif'];
      if (allow_extension.indexOf(file_extension.toLowerCase()) > -1) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  hide() {
    if (isPlatformBrowser(this.platformId)) {
      const present = new Date();
      localStorage.setItem(`notice_${this.article.cid}`, `${present.getFullYear()}-${present.getMonth()}-${present.getDate()}`);
      this.renderer2.setStyle(this.el.nativeElement, 'display', 'none');
    }
  }

  close() {
    this.renderer2.setStyle(this.el.nativeElement, 'display', 'none');
  }
}
